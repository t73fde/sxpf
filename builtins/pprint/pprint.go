//-----------------------------------------------------------------------------
// Copyright (c) 2023-present Detlef Stern
//
// This file is part of sxpf.
//
// sxpf is licensed under the latest version of the EUPL (European Union
// Public License). Please see file LICENSE.txt for your rights and obligations
// under this license.
//-----------------------------------------------------------------------------

// Package pprint provides some function to pretty-print objects.
package pprint

import (
	"io"
	"os"

	"codeberg.org/t73fde/sxpf"
	"codeberg.org/t73fde/sxpf/builtins"
	"codeberg.org/t73fde/sxpf/eval"
)

// Pretty writes the first argument to stdout.
func Pretty(_ *eval.Engine, _ sxpf.Environment, args []sxpf.Object) (sxpf.Object, error) {
	// Must be a BuiltinEEA, because this function has some side effects.
	err := builtins.CheckArgs(args, 0, 1)
	if err != nil {
		if len(args) == 0 {
			return sxpf.Nil(), nil
		}
		_, err = Print(os.Stdout, args[0])
	}
	return sxpf.Nil(), err
}

// Print object to given writer in a pretty way.
func Print(w io.Writer, obj sxpf.Object) (int, error) {
	written, err := doPrint(w, obj, 0)
	if err != nil {
		return written, err
	}
	n, err := w.Write(bEOL)
	return written + n, err
}

func doPrint(w io.Writer, obj sxpf.Object, indent int) (int, error) {
	if sxpf.IsNil(obj) {
		return w.Write(bNil)
	}
	if pair, isPair := sxpf.GetPair(obj); isPair {
		return doPrintList(w, pair, indent)
	}

	return sxpf.Print(w, obj)
}

func doPrintList(w io.Writer, lst *sxpf.Pair, indent int) (int, error) {
	written, err := w.Write(bOpen)
	if err != nil {
		return written, err
	}

	pos := 0
	mustIndent := false
	for node := lst; ; pos++ {
		if mustIndent {
			n, errNode := writeIndent(w, indent+4)
			written += n
			if err != nil {
				return written, errNode
			}
			mustIndent = false
		}

		n, errNode := doPrint(w, node.Car(), indent+1)
		written += n
		if err != nil {
			return written, errNode
		}

		cdr := node.Cdr()
		if sxpf.IsNil(cdr) {
			break
		}

		if next, isPair := sxpf.GetPair(cdr); isPair {
			contBytes, mi := calcContinuation(next, pos)
			nContBytes, errContBytes := w.Write(contBytes)
			written += nContBytes
			if errContBytes != nil {
				return written, errContBytes
			}
			node = next
			mustIndent = mi
			continue
		}

		n, errNode = w.Write(bDot)
		written += n
		if err != nil {
			return written, errNode
		}

		n, errNode = doPrint(w, cdr, indent)
		written += n
		if err != nil {
			return written, errNode
		}
		break
	}
	n, err := w.Write(bClose)
	return written + n, err
}

func calcContinuation(next *sxpf.Pair, pos int) ([]byte, bool) {
	if pos == 0 && next.Car().IsAtom() {
		return bSpace, false
	}
	return bEOL, true
}

var (
	bEOL   = []byte{'\n'}
	bNil   = []byte{'(', ')'}
	bOpen  = []byte{'('}
	bSpace = []byte{' '}
	bDot   = []byte{' ', '.', ' '}
	bClose = []byte{')'}
)

// 80 spaces
const spaces = "                                                                                "

func writeIndent(w io.Writer, indent int) (int, error) {
	var written int
	for indent > len(spaces) {
		n, err := io.WriteString(w, spaces)
		written += n
		if err != nil {
			return written, err
		}
		indent -= len(spaces)
	}
	n, err := io.WriteString(w, spaces[:indent])
	written += n
	return written, err
}
